variables:
  GIT_STRATEGY: clone
  REGISTRY: $CI_REGISTRY_IMAGE

cache:
  key: none
  paths: []
  policy: pull

stages:
  - docker
  - build
  - test
  - deploy

.docker: &docker
  stage: docker
  tags:
    - docker
    - linux
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker build
        --build-arg REGISTRY=$REGISTRY
        -t $CI_REGISTRY_IMAGE/$NAME:$TAG docker/$NAME/$TAG
    - docker push $CI_REGISTRY_IMAGE/$NAME:$TAG
  when: manual

.build: &build
  stage: build
  tags:
    - docker
    - linux
  script:
    - autoreconf -i
    - ./configure $CONFIGURE_OPTIONS
    - make
  artifacts:
    untracked: true

.test: &test
  stage: test
  tags:
    - docker
    - linux
  script:
    - touch aclocal.m4 configure config.status Makefile.in
    - make -t
    - make check
  artifacts:
    when: on_failure
    paths:
      - test/test-*.log
      - unit-tests/test-*.log
    expire_in: 1 week

.docker-gcc: &docker-gcc
  <<: *docker
  script:
    - docker build
        --build-arg __GNUC__=$__GNUC__
        -t $CI_REGISTRY_IMAGE/gcc:$__GNUC__ docker/gcc
    - docker push $CI_REGISTRY_IMAGE/gcc:$__GNUC__

docker-gcc:4:
  <<: *docker-gcc
  variables:
    __GNUC__: 4

build:gcc:4:
  image: $REGISTRY/gcc:4
  <<: *build

test:gcc:4:
  image: $REGISTRY/gcc:4
  dependencies:
    - build:gcc:4
  <<: *test

docker-gcc:5:
  <<: *docker-gcc
  variables:
    __GNUC__: 5

build:gcc:5:
  image: $REGISTRY/gcc:5
  <<: *build

test:gcc:5:
  image: $REGISTRY/gcc:5
  dependencies:
    - build:gcc:5
  <<: *test

docker-gcc:6:
  <<: *docker-gcc
  variables:
    __GNUC__: 6

build:gcc:6:
  image: $REGISTRY/gcc:6
  <<: *build

test:gcc:6:
  image: $REGISTRY/gcc:6
  dependencies:
    - build:gcc:6
  <<: *test

docker-gcc:7:
  <<: *docker-gcc
  variables:
    __GNUC__: 7

build:gcc:7:
  image: $REGISTRY/gcc:7
  <<: *build

test:gcc:7:
  image: $REGISTRY/gcc:7
  dependencies:
    - build:gcc:7
  <<: *test

docker-gcc:8:
  <<: *docker-gcc
  variables:
    __GNUC__: 8

build:gcc:8:
  image: $REGISTRY/gcc:8
  <<: *build

test:gcc:8:
  image: $REGISTRY/gcc:8
  dependencies:
    - build:gcc:8
  <<: *test

docker-gcc:9:
  <<: *docker-gcc
  variables:
    __GNUC__: 9

build:gcc:9:
  image: $REGISTRY/gcc:9
  <<: *build

test:gcc:9:
  image: $REGISTRY/gcc:9
  dependencies:
    - build:gcc:9
  <<: *test

docker-gcc:10:
  <<: *docker-gcc
  variables:
    __GNUC__: 10

build:gcc:10:
  image: $REGISTRY/gcc:10
  <<: *build

test:gcc:10:
  image: $REGISTRY/gcc:10
  dependencies:
    - build:gcc:10
  <<: *test

build:gcc:no-canfd:
  image: $REGISTRY/gcc:10
  <<: *build
  variables:
    CONFIGURE_OPTIONS: --disable-canfd

test:gcc:no-canfd:
  image: $REGISTRY/gcc:10
  dependencies:
    - build:gcc:no-canfd
  <<: *test

build:gcc:no-cxx:
  image: $REGISTRY/gcc:10
  <<: *build
  variables:
    CONFIGURE_OPTIONS: --disable-cxx

test:gcc:no-cxx:
  image: $REGISTRY/gcc:10
  dependencies:
    - build:gcc:no-cxx
  <<: *test

build:gcc:no-master:
  image: $REGISTRY/gcc:10
  <<: *build
  variables:
    CONFIGURE_OPTIONS: --disable-master

test:gcc:no-master:
  image: $REGISTRY/gcc:10
  dependencies:
    - build:gcc:no-master
  <<: *test

.docker-mingw-w64: &docker-mingw-w64
  <<: *docker
  script:
    - docker build
        --build-arg ARCH=$ARCH
        -t $CI_REGISTRY_IMAGE/$ARCH-w64-mingw32:$TAG docker/mingw-w64/$TAG
    - docker push $CI_REGISTRY_IMAGE/$ARCH-w64-mingw32:$TAG

docker:i686-w64-mingw32:
  <<: *docker-mingw-w64
  variables:
    ARCH: i686
    TAG: bullseye

build:i686-w64-mingw32:
  image: $REGISTRY/i686-w64-mingw32:bullseye
  <<: *build
  variables:
    CONFIGURE_OPTIONS: --host=i686-w64-mingw32 --disable-python

test:i686-w64-mingw32:
  image: $REGISTRY/i686-w64-mingw32:bullseye
  dependencies:
    - build:i686-w64-mingw32
  <<: *test

docker:x86_64-w64-mingw32:
  <<: *docker-mingw-w64
  variables:
    ARCH: x86_64
    TAG: bullseye

build:x86_64-w64-mingw32:
  image: $REGISTRY/x86_64-w64-mingw32:bullseye
  <<: *build
  variables:
    CONFIGURE_OPTIONS: --host=x86_64-w64-mingw32 --disable-python

test:x86_64-w64-mingw32:
  image: $REGISTRY/x86_64-w64-mingw32:bullseye
  dependencies:
    - build:x86_64-w64-mingw32
  <<: *test

.docker-crossbuild: &docker-crossbuild
  <<: *docker
  script:
    - docker build
        --build-arg ARCH=$ARCH
        --build-arg NAME=$NAME
        -t $CI_REGISTRY_IMAGE/$NAME:$TAG docker/crossbuild/$TAG
    - docker push $CI_REGISTRY_IMAGE/$NAME:$TAG

docker:arm-linux-gnueabihf:
  <<: *docker-crossbuild
  variables:
    ARCH: armhf
    NAME: arm-linux-gnueabihf
    TAG: bullseye

build:arm-linux-gnueabihf:
  image: $REGISTRY/arm-linux-gnueabihf:bullseye
  <<: *build
  variables:
    ARCH: armhf
    CONFIGURE_OPTIONS: --host=arm-linux-gnueabihf --disable-python

test:arm-linux-gnueabihf:
  image: $REGISTRY/arm-linux-gnueabihf:bullseye
  dependencies:
    - build:arm-linux-gnueabihf
  <<: *test
  variables:
    ARCH: armhf

docker:aarch64-linux-gnu:
  <<: *docker-crossbuild
  variables:
    ARCH: arm64
    NAME: aarch64-linux-gnu
    TAG: bullseye

build:aarch64-linux-gnu:
  image: $REGISTRY/aarch64-linux-gnu:bullseye
  <<: *build
  variables:
    ARCH: arm64
    CONFIGURE_OPTIONS: --host=aarch64-linux-gnu --disable-python

test:aarch64-linux-gnu:
  image: $REGISTRY/aarch64-linux-gnu:bullseye
  dependencies:
    - build:aarch64-linux-gnu
  <<: *test
  variables:
    ARCH: arm64

docker:arm-none-eabi:
  <<: *docker
  variables:
    NAME: arm-none-eabi
    TAG: bullseye

build:arm-none-eabi:
  image: $REGISTRY/arm-none-eabi:bullseye
  <<: *build
  variables:
    CFLAGS: -g -O2 -mcpu=cortex-m3 -D__NEWLIB__
    CONFIGURE_OPTIONS: --host=arm-none-eabi --disable-tools --disable-tests --disable-unit-tests --disable-python --disable-threads --disable-daemon
    CXXFLAGS: -g -O2 -mcpu=cortex-m3 -D__NEWLIB__
    LDFLAGS: --specs=nosys.specs

docker:build:
  <<: *docker
  variables:
    NAME: build
    TAG: bullseye

docker:doc:
  <<: *docker
  variables:
    NAME: doc
    TAG: bullseye

doc:
  stage: build
  tags:
    - docker
    - linux
  image: $REGISTRY/doc:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - ./configure
    - make html
  artifacts:
    paths:
      - doc/
    expire_in: 1 week

docker:cpplint:
  <<: *docker
  variables:
    NAME: cpplint
    TAG: bullseye

cpplint:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/cpplint:bullseye
  script:
    - cpplint.py
        --quiet
        --extensions=cpp
        $(find include -name *.hpp)
        $(find src -name *.cpp)

docker:clang-format:
  <<: *docker
  variables:
    NAME: clang-format
    TAG: bullseye

clang-format:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/clang-format:bullseye
  script:
    - clang-format-10
        -n
        --Werror
        $(find include -name *.h)
        $(find src -name *.c)
        $(find unit-tests -name *.cpp -o -name *.hpp)
# TODO - enable C++ clang-format check

docker:valgrind:
  <<: *docker
  variables:
    NAME: valgrind
    TAG: bullseye

valgrind:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/valgrind:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - ./configure --disable-python
    - make
    - make check-valgrind-memcheck
  artifacts:
    when: on_failure
    paths:
      - test/test-*.log
      - unit-tests/test-*.log
    expire_in: 1 week

docker:lcov:
  <<: *docker
  variables:
    NAME: lcov
    TAG: bullseye

lcov:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/lcov:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - ./configure --enable-code-coverage --disable-shared
    - make
    - make check-code-coverage
    - lcov --summary coverage.info --rc lcov_branch_coverage=1
  artifacts:
    paths:
      - coverage/
    expire_in: 1 week

docker:scan-build:
  <<: *docker
  variables:
    NAME: scan-build
    TAG: bullseye

scan-build:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/scan-build:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - scan-build ./configure --disable-python
    - scan-build --keep-cc -o ./scan-build make check
  artifacts:
    when: on_failure
    paths:
      - scan-build/
    expire_in: 1 week

docker:cppcheck-unix64:
  <<: *docker
  variables:
    NAME: cppcheck-unix64
    TAG: bullseye

cppcheck-unix64:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/cppcheck-unix64:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - ./configure --disable-python
    - cppcheck
        -D_GNU_SOURCE -D__linux__=1 -DHAVE_CONFIG_H
        --enable=warning,style,performance,portability,missingInclude
        --error-exitcode=1
        --force
        -I. -I./include
        --inline-suppr
        --platform=unix64
        --quiet
        --std=c11 --std=c++11
        --library=std --library=gnu
        --suppressions-list=suppressions.txt
        -isrc/io2/win32
        include src

docker:cppcheck-win64:
  <<: *docker
  variables:
    NAME: cppcheck-win64
    TAG: bullseye

cppcheck-win64:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/cppcheck-win64:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - ./configure --host=x86_64-w64-mingw32 --disable-python
    - cppcheck
        -D_WIN32=1 -D_WIN64=1 -D__MINGW32__=1 -D__MINGW64__=1 -DHAVE_CONFIG_H
        -U__linux__
        --enable=warning,style,performance,portability,missingInclude
        --error-exitcode=1
        --force
        -I. -I./include
        --inline-suppr
        --platform=win64
        --quiet
        --std=c11 --std=c++11
        --library=std --library=gnu --library=windows
        --suppress=va_list_usedBeforeStarted
        --suppressions-list=suppressions.txt
        -isrc/io2/linux
        include src
  when: manual

docker:coverity:
  <<: *docker
  script:
    - docker build
        --build-arg REGISTRY=$REGISTRY
        --build-arg COVERITY_PROJECT=$COVERITY_PROJECT
        --build-arg COVERITY_TOKEN=$COVERITY_TOKEN
        -t $CI_REGISTRY_IMAGE/coverity:bullseye docker/coverity/bullseye
    - docker push $CI_REGISTRY_IMAGE/coverity:bullseye

coverity:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/coverity:bullseye
  dependencies: []
  script:
    - autoreconf -i
    - ./configure --disable-python
    - cov-build --dir cov-int make check
  after_script:
    - tar caf $CI_PROJECT_NAME.xz cov-int
    - curl --form token=$COVERITY_TOKEN
        --form email=$COVERITY_EMAIL
        --form file=@$CI_PROJECT_NAME.xz
        --form version="$(git describe --abbrev=0 --tags | sed s/^v//)"
        --form description="$CI_COMMIT_REF_NAME"
        https://scan.coverity.com/builds?project=$COVERITY_PROJECT
  when: manual

docker:sonarcloud:
  <<: *docker
  script:
    - docker build
        --build-arg REGISTRY=$REGISTRY
        --build-arg SONAR_HOST_URL=$SONAR_HOST_URL
        --build-arg SONAR_TOKEN=SONAR_TOKEN
        -t $CI_REGISTRY_IMAGE/sonarcloud:bullseye docker/sonarcloud/bullseye
    - docker push $CI_REGISTRY_IMAGE/sonarcloud:bullseye

sonarcloud:
  stage: test
  tags:
    - docker
    - linux
  image: $REGISTRY/sonarcloud:bullseye
  dependencies: []
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  script:
    - autoreconf -i
    - ./configure --disable-python
    - /opt/sonar-build-wrapper/build-wrapper-linux-x86-64 --out-dir bw-output make check
    - /opt/sonar-scanner/bin/sonar-scanner
           -Dsonar.host.url="${SONAR_HOST_URL}"
           -Dsonar.token="${SONAR_TOKEN}"
           -Dsonar.projectKey="${SONAR_PROJECT_KEY}"
           -Dsonar.organization="${SONAR_ORGANIZATION}"
           -Dsonar.projectName="Lely CANopen"
           -Dsonar.links.homepage="${CI_PROJECT_URL}"
           -Dsonar.links.ci="${CI_PROJECT_URL}/pipelines"
           -Dsonar.links.scm="${CI_PROJECT_URL}"
           -Dsonar.links.issue="${CI_PROJECT_URL}/issues"
           -Dsonar.sources=include,src,tools
           -Dsonar.tests=test,unit-tests
           -Dsonar.cfamily.build-wrapper-output=bw-output
  when: manual

pages:
  stage: deploy
  only:
    - master
  tags:
    - docker
    - linux
  image: alpine:latest
  dependencies:
    - doc
    - lcov
  script:
    - mkdir -p public
    - mv coverage public/lcov
    - mv doc/html public/doxygen
  artifacts:
    paths:
      - public
    expire_in: 1 weeks
