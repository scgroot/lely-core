---
Language: Cpp
BasedOnStyle: Google
AlignEscapedNewlines: DontAlign
AlwaysBreakAfterReturnType: AllDefinitions
DerivePointerAlignment: false
ForEachMacros:
  - co_reenter
PointerAlignment: Left
SortIncludes: false
Standard: Cpp11
...
